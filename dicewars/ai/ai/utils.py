from dicewars.ai.utils import possible_attacks, probability_of_successful_attack, attack_succcess_probability
from dicewars.client.ai_driver import BattleCommand, EndTurnCommand, TransferCommand
import os
from pprint import pprint
import numpy as np
import time

DICE_LIMIT = 8
ATTACK = 0
TRANSFER = 1
MAX_DICE = 8

# Restrain transfers by path history (also omit neighbours)
# Restrain attacks and transfers by distance from another nodes expanding

# Area indexes
DICE = 0
OWNER = 1
ID = 2
# Attack types
ATTACK_S = 0
ATTACK_F = 1
TRANSFER = 2

class GameSearch:
    '''
    Class representing the state of game as node and search tree
    '''
    def __init__(self, board, board_neighbours, player_name, possible_transfers):
        self.possible_transfers = possible_transfers
        self.player_name = player_name
        self.board = board
        self.board_neighbours = board_neighbours
        self.moves = [()]
        # Generate new states (from actual state) Empty tuple is empty move
        self.states = [self.board.copy()]
        self.paths = [[]]
        self.hashmap = {}


    @classmethod
    def from_board(cls, game_board, player_name, possible_transfers):
        '''
        Creates GameSearch instance from dicewars board class
        '''
        board, board_neighbours = [], []
        for k in game_board.areas:
            area = game_board.areas[k]
            # Board will be 2d array, on index 0 are dice, on index 1 is name
            board.append(np.array([int(area.get_dice()), int(area.get_owner_name()), int(area.get_name()) - 1]))
            board_neighbours.append(np.array(area.get_adjacent_areas_names()) - 1)
        board, board_neighbours = np.array(board), np.array(board_neighbours)
        return cls(board, board_neighbours, player_name, possible_transfers)

    # Some kind od backtracking
    def create_game_tree(self):
        '''
        Create game search tree from self

        Compute just from the beggining of turn, apply for each move then
        '''
        # Actual state in self.states (only 1)
        # Search, when all generated were closed, stop searching
        ignore_states = 0
        start = time.time()
        states_generated, states_closed = 1, 0
        while states_closed < states_generated:
            # Load new state and save it into hashmap
            self.board = self.states[states_closed]
            # Expand the state
            next_states, next_paths, ignored_states = self.generate_new_states(states_closed)
            # Some states were generated, 1 was closed
            ignore_states += ignored_states
            states_generated += len(next_states)
            states_closed += 1
            # Add generated states
            self.states += next_states
            self.paths += next_paths
            if states_closed % 1000 == 0:
                print(f'Closed: {states_closed}, Generated: {states_generated}, Ignored {ignore_states}')
                print(f'Time: {time.time() - start}')
                start = time.time()
        print(f'Closed: {states_closed}, Generated: {states_generated}')

    def apply_state(self, move):
        new_board = self.board.copy()
        for change in move[:2]:
            new_board[change[ID]] = change
        return new_board

    def generate_new_states(self, father_state):
        # Generate new attack possibilities
        attacks = self.get_all_possible_attacks_from_state(father_state)
        ignored = 0
        # If transfers left, expand also transfer possibilities
        if sum([1 for move in self.paths[father_state][1:] if move[1][OWNER] == move[0][OWNER]]) < self.possible_transfers:
            transfers = self.get_all_possible_transfers_from_state(father_state)
        else:
            transfers = []
        new_states = []
        new_paths = []
        # Create new states from all possible moves
        for move in attacks + transfers:
            # Create the new state
            new_state = self.apply_state(move)
            # Look if already existing state was not generated
            if not self.hashmap.get(new_state.tostring()):
                new_states.append(new_state)
                new_paths.append(self.paths[father_state] + [move])
                self.hashmap[new_state.tostring()] = True
            else:
                ignored += 1
        return new_states, new_paths, ignored

    def get_neighbors(self, area, friendly=True):
        return [self.board[neigh] for neigh in self.board_neighbours[area[ID]] if (self.board[neigh, ID] == self.player_name) == friendly]

    def get_my_areas(self):
        return [area for area in self.board[:] if area[OWNER] == self.player_name]

    def get_all_possible_attacks_from_state(self, father_state):
        attacks = []
        for area in self.get_my_areas():
            if area[DICE] > 1:
                for neigh_area in self.get_neighbors(area, friendly=False):
                    # Take in accaunt only attacks with at least the same force as destination area
                    if area[DICE] >= neigh_area[DICE]:
                        # Attacks are src and dst areas after attack: [DICE, OWNER, ID]
                        attacks.append((
                            # Succesful
                            (1, area[OWNER], area[ID]),
                            (area[DICE] - 1, area[OWNER], neigh_area[ID]),
                            father_state
                        ))
                        attacks.append((
                            (1, area[OWNER], area[ID]),
                            (neigh_area[DICE], neigh_area[OWNER], neigh_area[ID]),
                            father_state
                        ))
        return attacks
    
    def get_all_possible_transfers_from_state(self, father_state):
        transfers = []
        for area in self.get_my_areas():
            if area[DICE] > 1:
                for neigh_area in self.get_neighbors(area, friendly=True):
                    if neigh_area[DICE] < 8:
                        # Transfers are src and dst areas after attack: [DICE, OWNER, ID]
                        transfers.append((
                            (max(area[DICE]-MAX_DICE+neigh_area[DICE], 1), area[OWNER], area[ID]),
                            (min(MAX_DICE, area[DICE]+neigh_area[DICE]-1), neigh_area[OWNER], neigh_area[ID]),
                            father_state
                        ))
        return transfers


class Attack:
    def __init__(self, src, dst, board):
        self.src = src
        self.dst = dst
        self.board = board
        self.dice_advantage = src.get_dice() - dst.get_dice()
        self._success_p = 0
        self.transfers_needed = []
        self.potential_gain = 0

    @property
    def is_favorable(self):
        return self.success_p >= FAVORABLE_PROBABILITY

    @property
    def success_p(self):
        # Calculate probability of winning, include possible transfers 
        return attack_succcess_probability(min(src.get_dice() + self.potential_gain, 8), dst.get_dice())

    def attack(self):
        return BattleCommand(self.src.get_name(), self.dst.get_name())


def get_attack_probabilities(board, player_name):
    attacks = [Attack(src, dst, board) for src, dst in possible_attacks(board, player_name)]
    attacks = [attack for attack in sorted(attacks, key=lambda attack: attack.success_p, reverse=True) if attack.src.get_dice() > 1]
    return attacks