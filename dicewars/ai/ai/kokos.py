import logging
import traceback

from dicewars.client.ai_driver import BattleCommand, EndTurnCommand
from dicewars.ai.ai.utils import GameSearch

class AI:
    def __init__(self, player_name, board, players_order, max_transfers):
        self.player_name = player_name
        self.logger = logging.getLogger('AI')
        try:
            self.search = GameSearch.from_board(board, player_name, max_transfers)
        except Exception as e:
            print(traceback.format_exc())

    def ai_turn(self, board, nb_moves_this_turn, nb_transfers_this_turn, nb_turns_this_game, time_left):
        try:
            self.search.create_game_tree()
        except Exception as e:
            print(traceback.format_exc())
        return EndTurnCommand()
